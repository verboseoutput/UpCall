An Android app designed to familiarize myself with [Kotlin](https://developer.android.com/kotlin/index.html). 

**Application Purpose**
Keep detailed statistics of ulatimate players across teams and seasons.

**Requirements**
 - input detailed statistics on passes, goals and turns rapidly
 - View data on a player, team, and season basis
 - handle multiple users
 
**Future Plans**
 - Store data using google cloud services
 - access data using a web portal
  
**Git Branch Plan**
 - Master
   - contains only released code
   - code passes all tests
 - Dev
   - all code compiles
 - issue
   - one issue branch per issue being solved
   - code has no guarantees on whether it compiles
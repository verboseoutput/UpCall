package com.verboseoutput.upcall.fragment


import android.database.Cursor
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.LoaderManager
import android.support.v4.content.CursorLoader
import android.support.v4.content.Loader
import android.view.*
import android.widget.ListView
import android.widget.PopupMenu
import android.widget.SimpleCursorAdapter
import com.verboseoutput.upcall.R
import com.verboseoutput.upcall.logic.StatsContract
import com.verboseoutput.upcall.logic.logger
import org.jetbrains.anko.find
import java.util.logging.Level


class RosterFragment : Fragment(), LoaderManager.LoaderCallbacks<Cursor> {
    private var mTeamId: Long = -1
    private var mSeasonId: Long = -1
    private lateinit var mRosterAdapter: SimpleCursorAdapter
    private val mProjection = arrayOf(
            StatsContract.PlayerInfo.COLUMN_NAME_ID,
            StatsContract.PlayerInfo.COLUMN_NAME_FIRST_NAME,
            StatsContract.PlayerInfo.COLUMN_NAME_LAST_NAME)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments != null) {
            mTeamId = arguments.getLong(ARG_TEAM_ID, -1)
            mSeasonId = arguments.getLong(ARG_SEASON_ID, -1)
        }

        // init roster loader
        loaderManager.initLoader(LoaderType.ROSTER.id, null, this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_roster, container, false)

        // Setup Display Information
        val mRosterFromColumns = arrayOf(StatsContract.PlayerInfo.COLUMN_NAME_FIRST_NAME,
                StatsContract.PlayerInfo.COLUMN_NAME_LAST_NAME)
        val mToViews = intArrayOf(android.R.id.text1, android.R.id.text2)
        mRosterAdapter = SimpleCursorAdapter(
                context,
                android.R.layout.simple_list_item_2,
                null,
                mRosterFromColumns,
                mToViews,
                0
        )
        val rosterList = view.find<ListView>(R.id.roster_list)
        rosterList.adapter = mRosterAdapter

        rosterList.setOnItemClickListener { _, view, position, _ ->

            val popupMenu = PopupMenu(context, view)
            popupMenu.setOnMenuItemClickListener { item ->
                when(item.itemId) {
                    R.id.remove_player -> {
                        val cursor = mRosterAdapter.cursor
                        cursor.moveToPosition(position)
                        val columnName = "${StatsContract.Roster.COLUMN_NAME_PLAYER_ID} AS ${StatsContract.PlayerInfo.COLUMN_NAME_ID}"
                        val playerId = cursor.getLong(mProjection.indexOf(columnName))
                        removePlayer(mTeamId, mSeasonId, playerId)
                        true
                    }
                    else -> {
                        LOG.severe("Unkown popup menu selection")
                        true
                    }
                }
            }


            popupMenu.menuInflater.inflate(R.menu.roster_actions, popupMenu.menu)
            popupMenu.show()
        }

        return view
    }

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<Cursor>? {
        var cursorLoader: CursorLoader? = null
        when (id) {
            LoaderType.ROSTER.id -> {
                val select = """
                        ${StatsContract.Roster.COLUMN_NAME_TEAM_ID}=? AND
                        ${StatsContract.Roster.COLUMN_NAME_SEASON_ID}=?
                     """

                val selectArgs: Array<String> = arrayOf(mTeamId.toString(),mSeasonId.toString())
                cursorLoader = CursorLoader (
                        context,
                        StatsContract.Roster.CONTENT_PLAYER_URI,
                        mProjection,
                        select,
                        selectArgs,
                        null
                )
            }
            else -> TeamPageFragment.LOG.log(Level.SEVERE, "Unknown loader id")
        }

        return cursorLoader
    }

    override fun onLoadFinished(loader: Loader<Cursor>?, data: Cursor?) {
        mRosterAdapter.changeCursor(data)
    }

    override fun onLoaderReset(p0: Loader<Cursor>?) {
        mRosterAdapter.changeCursor(null)
    }

    private fun removePlayer(teamId: Long, seasonId: Long, playerId: Long) {
        val selection = """
            ${StatsContract.Roster.COLUMN_NAME_TEAM_ID}=? AND
            ${StatsContract.Roster.COLUMN_NAME_SEASON_ID}=? AND
            ${StatsContract.Roster.COLUMN_NAME_PLAYER_ID}=?
            """
        val selectionArgs = arrayOf(teamId.toString(), seasonId.toString(), playerId.toString())

        activity.contentResolver.delete(
                StatsContract.Roster.CONTENT_URI,
                selection,
                selectionArgs
        )
    }

    enum class LoaderType(val id: Int) {
        ROSTER(0)
    }

    companion object {
        val LOG = logger()
        private val ARG_TEAM_ID = "teamIdParam"
        private val ARG_SEASON_ID = "seasonIdParam"

        fun newInstance(teamId: Long, seasonId: Long): RosterFragment {
            val fragment = RosterFragment()
            val args = Bundle()
            args.putLong(ARG_TEAM_ID, teamId)
            args.putLong(ARG_SEASON_ID, seasonId)
            fragment.arguments = args
            return fragment
        }
    }
}
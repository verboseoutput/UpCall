package com.verboseoutput.upcall.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.verboseoutput.upcall.R


/**
 * A simple [Fragment] subclass.
 */
class RecentGamesFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_recent_games, container, false)
    }

    companion object {
        fun newInstance(): RecentGamesFragment {
            return RecentGamesFragment()
        }
    }

}
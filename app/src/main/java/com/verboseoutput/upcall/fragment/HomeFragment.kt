package com.verboseoutput.upcall.fragment

import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.verboseoutput.upcall.R
import com.verboseoutput.upcall.dialog.EditProfile
import org.jetbrains.anko.find
import org.w3c.dom.Text


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [HomeFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment() {
    private var mFirstName: String? = null
    private var mLastName: String? = null

    private var mListener: ShowNewDialogListener? = null

    interface ShowNewDialogListener {
        fun showNewDialog(dialog: DialogFragment)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mFirstName = arguments.getString(ARG_FIRST_NAME)
            mLastName = arguments.getString(ARG_LAST_NAME)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        val firstNameView = view.find<TextView>(R.id.home_first_name)
        val lastNameView = view.find<TextView>(R.id.home_last_name)

        if (mFirstName!!.isEmpty()) {
            firstNameView.setTypeface(firstNameView.typeface, Typeface.ITALIC)
        } else {
            firstNameView.text = mFirstName
        }
        if (mLastName!!.isEmpty()) {
            lastNameView.setTypeface(lastNameView.typeface, Typeface.ITALIC)
        } else {
            lastNameView.text = mLastName
        }

        val editProfileButton = view.find<Button>(R.id.button_edit_profile)
        editProfileButton.setOnClickListener {
            mListener?.showNewDialog(EditProfile.newInstance(mFirstName.toString(), mLastName.toString()))
        }


        return view
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ShowNewDialogListener) {
            mListener = context
        } else {
            throw RuntimeException(context.toString() + " must implement showNewDialogListener")
        }
    }

    fun updateFirstName(firstName: String) {
        mFirstName = firstName
        view?.find<TextView>(R.id.home_first_name)?.text = firstName
        val firstNameView = view?.find<TextView>(R.id.home_first_name)
        if(firstName.isNotEmpty()) firstNameView?.setTypeface(firstNameView.typeface, Typeface.NORMAL)
    }

    fun updateLastName(lastName: String) {
        mLastName = lastName
        view?.find<TextView>(R.id.home_last_name)?.text = lastName
        val lastNameView = view?.find<TextView>(R.id.home_last_name)
        if(lastName.isNotEmpty()) lastNameView?.setTypeface(lastNameView.typeface, Typeface.NORMAL)
    }

    companion object {
        private val ARG_FIRST_NAME = "first"
        private val ARG_LAST_NAME = "last"


        fun newInstance(firstName: String, lastName: String): HomeFragment {
            val fragment = HomeFragment()
            val args = Bundle()
            args.putString(ARG_FIRST_NAME, firstName)
            args.putString(ARG_LAST_NAME, lastName)
            fragment.arguments = args
            return fragment
        }
    }
}
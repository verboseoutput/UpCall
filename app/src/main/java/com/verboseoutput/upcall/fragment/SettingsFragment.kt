package com.verboseoutput.upcall.fragment

import android.os.Bundle
import android.preference.PreferenceFragment
import com.verboseoutput.upcall.R

 class SettingsFragment : PreferenceFragment() {
     override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences)
    }
}

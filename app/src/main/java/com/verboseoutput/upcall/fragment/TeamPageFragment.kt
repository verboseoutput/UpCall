package com.verboseoutput.upcall.fragment

import android.content.ContentUris
import android.content.Context
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.TabLayout
import android.support.v4.app.*
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.*
import com.verboseoutput.upcall.R
import com.verboseoutput.upcall.dialog.AddPlayer
import com.verboseoutput.upcall.logic.StatsContract
import com.verboseoutput.upcall.logic.logger
import org.jetbrains.anko.find


class TeamPageFragment : Fragment() {
    private var mTeamName: String? = null

    private var mTeamId: Long = -1
    private var mSeasonId: Long = -1
    private var mListener: ShowNewDialogListener? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        if (arguments != null) {
            mTeamId = arguments.getLong(ARG_TEAM_ID, -1)
            mSeasonId = arguments.getLong(ARG_SEASON_ID, -1)
        }

        val projection = arrayOf(
                StatsContract.TeamInfo.COLUMN_NAME_TEAM_ID,
                StatsContract.TeamInfo.COLUMN_NAME_TEAM_NAME,
                StatsContract.TeamInfo.COLUMN_NAME_CITY,
                StatsContract.TeamInfo.COLUMN_NAME_STATE
        )

        val teamUri = ContentUris.withAppendedId(StatsContract.TeamInfo.CONTENT_URI, mTeamId)
        val teamCursor = activity.contentResolver.query(
                teamUri,
                projection,
                null, null, null
        )

        val actionBar = (activity as AppCompatActivity).supportActionBar
        if(teamCursor.moveToFirst()) {
            mTeamName = teamCursor.getString(teamCursor.getColumnIndex(StatsContract.TeamInfo.COLUMN_NAME_TEAM_NAME))
            actionBar?.title = mTeamName
        } else {
            actionBar?.title = "UNKNOWN TEAM"
            LOG.severe("FAILED TO FIND team based on teamid: $mTeamId")
        }

        teamCursor.close()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_team_page, container, false)

        // Setup Tab Layout
        val tabLayout = view.find<TabLayout>(R.id.team_tab_layout)
        for(tab in Tabs.values()) {
            tabLayout.addTab(tabLayout.newTab())
        }
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL

        val adapter = MyAdapter(childFragmentManager, tabLayout.tabCount)

        val viewPager = view.find<ViewPager>(R.id.team_pager)
        viewPager.adapter = adapter

        tabLayout.setupWithViewPager(viewPager)

        // Setup FAB
        val fab = view.find<FloatingActionButton>(R.id.team_page_fab)
        fab.setOnClickListener {
            LOG.info("Start the New Game Activity!")
        }
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ShowNewDialogListener) {
            mListener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.team_page_actions, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean = when(item?.itemId) {
        R.id.action_add_player -> {
            // TODO select a previously created player currently in the database
            mListener?.showNewDialog(AddPlayer.newInstance(mTeamId, mSeasonId))
            true
        }
        else -> activity.onOptionsItemSelected(item)
    }

    interface ShowNewDialogListener {
        fun showNewDialog(dialog: DialogFragment)
    }

    inner class MyAdapter(fm: FragmentManager, numOfTabs: Int): FragmentPagerAdapter(fm) {
        private var mNumOfTabs: Int = numOfTabs

        override fun getCount() : Int {
            return mNumOfTabs
        }

        override fun getItem(position: Int): Fragment? = when(position) {
            in 0 until Tabs.values().size -> Tabs.values()[position].fragment(mTeamId, mSeasonId)
            else -> {
                LOG.severe("Unknown Tab at position: $position")
                null
            }
        }

        override fun getPageTitle(position: Int): CharSequence = when(position) {
            in 0 until Tabs.values().size -> Tabs.values()[position].title()
            else -> {
                LOG.severe("Unknown Tab at position: $position")
                ""
            }
        }
    }

    enum class Tabs {
        GAMES {
            override fun title(): String = "Games"
            override fun fragment(teamId: Long, seasonId: Long): Fragment = RecentGamesFragment.newInstance(/*TODO probably going to need teamID and seasonID as well*/)
        },
        LEADER_BOARD {
            override fun title(): String = "Leader Board"
            override fun fragment(teamId: Long, seasonId: Long): Fragment = LeaderBoardFragment.newInstance(/*TODO probably going to need teamID and seasonID as well*/)
        },
        ROSTER {
            override fun title(): String = "Roster"
            override fun fragment(teamId: Long, seasonId: Long): Fragment = RosterFragment.newInstance(teamId, seasonId)
        };
        abstract fun title(): String
        abstract fun fragment(teamId: Long, seasonId: Long): Fragment?
    }

    companion object {
        private val ARG_TEAM_ID = "teamIdParam"
        private val ARG_SEASON_ID = "seasonIdParam"

        fun newInstance(teamId: Long, seasonId: Long): TeamPageFragment {
            val fragment = TeamPageFragment()
            val args = Bundle()
            args.putLong(ARG_TEAM_ID, teamId)
            args.putLong(ARG_SEASON_ID, seasonId)
            fragment.arguments = args
            return fragment
        }

        val LOG = logger()
    }
}

package com.verboseoutput.upcall

import android.app.Activity
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.verboseoutput.upcall.logic.logger
import java.util.ArrayList
import com.firebase.ui.auth.ErrorCodes
import com.firebase.ui.auth.IdpResponse
import android.content.Intent
import android.support.annotation.MainThread
import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.view.View
import org.jetbrains.anko.find


class LoginActivity : AppCompatActivity() {
    companion object {
        private val TAG = "LogActivity"
        val LOG = logger()

        fun createIntent(context: Context): Intent {
            val intent = Intent()
            intent.setClass(context, LoginActivity::class.java)
            return intent
        }
    }

    private val RC_SIGN_IN = 100
    private var mRootView: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        mRootView = find<View>(R.id.login_root_view)

        val auth = FirebaseAuth.getInstance()
        if(auth.currentUser == null) {
            val selectedProviders = ArrayList<AuthUI.IdpConfig>()
            selectedProviders.add(AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build())

            selectedProviders.add(AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build())

            startActivityForResult(
                    AuthUI.getInstance().createSignInIntentBuilder()
                            .setAvailableProviders(selectedProviders)
                            .setIsSmartLockEnabled(!BuildConfig.DEBUG)
                            .build(),
                    RC_SIGN_IN)
        } else {
            startMainActivity()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        // RC_SIGN_IN is the request code you passed into startActivityForResult(...) when starting the sign in flow.
        if (requestCode == RC_SIGN_IN) {
            val response = IdpResponse.fromResultIntent(data)
            LOG.info("response idp token ${response?.idpToken}")

            // Successfully signed in
            if (resultCode == Activity.RESULT_OK) {
                startMainActivity()
                finish()
                return
            } else {
                // Sign in failed
                if (response == null) {
                    // User pressed back button
                    showSnackbar(R.string.sign_in_cancelled)
                    return
                }

                if (response.errorCode == ErrorCodes.NO_NETWORK) {
                    showSnackbar(R.string.no_internet_connection)
                    return
                }

                if (response.errorCode == ErrorCodes.UNKNOWN_ERROR) {
                    showSnackbar(R.string.unknown_error)
                    return
                }
            }

            showSnackbar(R.string.unknown_sign_in_response)
        }
    }

    @MainThread
    private fun showSnackbar(@StringRes errorMessageRes: Int) {
        Snackbar.make(mRootView!!, errorMessageRes, Snackbar.LENGTH_LONG).show()
    }

    private fun startMainActivity() {
        startActivity(MainActivity.createIntent(this))
    }

}

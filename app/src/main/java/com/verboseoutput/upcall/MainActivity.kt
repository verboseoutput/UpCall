package com.verboseoutput.upcall

import android.content.ContentUris
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.database.MatrixCursor
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.*
import android.support.v4.content.CursorLoader
import android.support.v4.content.Loader
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import com.firebase.ui.auth.AuthUI
import com.verboseoutput.upcall.logic.StatsContract
import com.verboseoutput.upcall.logic.logger
import org.jetbrains.anko.find
import java.util.*
import com.google.firebase.auth.FirebaseAuth
import com.verboseoutput.upcall.dialog.EditProfile
import com.verboseoutput.upcall.dialog.NewGame
import com.verboseoutput.upcall.dialog.AddPlayer
import com.verboseoutput.upcall.dialog.NewTeam
import com.verboseoutput.upcall.fragment.HomeFragment
import com.verboseoutput.upcall.fragment.TeamPageFragment
import kotlin.collections.ArrayList
import kotlin.properties.Delegates

class MainActivity : AppCompatActivity(), LoaderManager.LoaderCallbacks<Cursor>,
        NewTeam.NewTeamDialogListener,
        NewGame.NewGameDialogListener,
        AddPlayer.NewPlayerDialogListener,
        EditProfile.EditProfileDialogListener,

        TeamPageFragment.ShowNewDialogListener,
        HomeFragment.ShowNewDialogListener{

    companion object {
        val LOG = logger()

        fun createIntent(context: Context): Intent {
            val startIntent = Intent()
            return startIntent.setClass(context, MainActivity::class.java)
        }
    }

    private var mHomeFragment: HomeFragment? = null

    private var mAuth: FirebaseAuth? = null
    private val mRosterProjection = arrayOf(StatsContract.Roster.COLUMN_NAME_TEAM_ID,
                                            StatsContract.Roster.COLUMN_NAME_SEASON_ID,
                                            StatsContract.TeamInfo.COLUMN_NAME_TEAM_NAME)
    private lateinit var mTeamAdapter: SimpleCursorAdapter
    private var mPlayerId: Int = -1
    private var mPlayerFirstName: String by Delegates.observable("") {
        _, _, newValue -> mHomeFragment?.updateFirstName(newValue)
    }
    private var mPlayerLastName: String by Delegates.observable("") {
        _, _, newValue -> mHomeFragment?.updateLastName(newValue)
    }
    private var mFirebaseId: String = ""
    private var mTeamsAndSeasons: HashMap<Int, ArrayList<Int>> = hashMapOf()
    private var mTeamList: MatrixCursor? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Create Toolbar
        val toolbar = find<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)




        // Nav Drawer
        val drawer = find<DrawerLayout>(R.id.drawer_layout)

        // Add Nav Options to Drawer
        val navOptions = find<ListView>(R.id.nav_options)
        navOptions.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
                resources.getStringArray(R.array.nav_headers))

        // Set Nav Selection Listener
        navOptions.setOnItemClickListener { parent, _, position, _ ->

            val item = parent.adapter.getItem(position)
            when(item.toString()) {
                getString(R.string.nav_header_home) -> {
                    val fragment = HomeFragment.newInstance(mPlayerFirstName, mPlayerLastName)
                    mHomeFragment = fragment
                    showNewFragment(fragment)
                }
                else ->  LOG.info("Unknown Navigation Option")
            }

            drawer.closeDrawers()
        }


        // Add Teams to Drawer
        val columnNames = arrayOf(StatsContract.TeamInfo.COLUMN_NAME_TEAM_ID, StatsContract.TeamInfo.COLUMN_NAME_TEAM_NAME)
        mTeamList = MatrixCursor(columnNames)

        val mFromColumns = arrayOf(StatsContract.TeamInfo.COLUMN_NAME_TEAM_NAME)
        val mToViews = intArrayOf(android.R.id.text1)

        mTeamAdapter = SimpleCursorAdapter(
                this,
                android.R.layout.simple_list_item_1,
                null,
                mFromColumns,
                mToViews,
                0
        )

        val navTeamList = find<ListView>(R.id.nav_team_list)
        navTeamList.adapter = mTeamAdapter

        // Set Listener for team selection
        navTeamList.setOnItemClickListener { _, _, position, _ ->
            val cursor = mTeamAdapter.getItem(position) as Cursor
            val teamId = cursor.getInt(columnNames.indexOf(StatsContract.TeamInfo.COLUMN_NAME_TEAM_ID))

            val currentSeason = mTeamsAndSeasons[teamId]!!.get(0)
            showNewFragment(TeamPageFragment.newInstance(teamId.toLong(), currentSeason.toLong()))

            drawer.closeDrawers()
        }

        // sync with action bar
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        // Set TextView Listeners in Nav Drawer
        val newTeam = find<TextView>(R.id.nav_new_team)
        newTeam.setOnClickListener {
            showNewDialog(NewTeam())
            drawer.closeDrawers()
        }

        val settings = find<TextView>(R.id.nav_settings)
        settings.setOnClickListener {
            drawer.closeDrawers()
            // TODO open PreferenceActivity since apparently it is its own special thing
        }

        // Get the Firebase Authorization instance for user sign in
        mAuth = FirebaseAuth.getInstance()
    }

    override fun onStart() {
        super.onStart()
        val currentUser = mAuth!!.currentUser

        // Haven't signed in yet, open the Login Activity
        if (currentUser == null) {
            startActivity(LoginActivity.createIntent(this))
            finish()
            return
        } else {
            mFirebaseId = currentUser.uid

            // Get Our PlayerID based on the firebase UID
            val selection = "${StatsContract.PlayerInfo.COLUMN_NAME_FIREBASE_UID}=?"
            val selectionArgs = arrayOf(mFirebaseId)
            val projection = arrayOf( StatsContract.PlayerInfo.COLUMN_NAME_ID,
                                      StatsContract.PlayerInfo.COLUMN_NAME_FIREBASE_UID,
                                      StatsContract.PlayerInfo.COLUMN_NAME_FIRST_NAME,
                                      StatsContract.PlayerInfo.COLUMN_NAME_LAST_NAME)
            val playerCursor = contentResolver.query(
                    StatsContract.PlayerInfo.CONTENT_URI,
                    projection,
                    selection,
                    selectionArgs,
                    null)

            // Player Exists, pass the playerID to the loader manager so we can get this users
            // teams
            val isNewPlayer: Boolean
            if(playerCursor.moveToFirst()) {
                isNewPlayer = false
                getPlayerProfile(playerCursor)

                val loaderArgs = Bundle()
                val args = arrayOf(mPlayerId.toString())
                loaderArgs.putStringArray("playerid", args)
                supportLoaderManager.initLoader(0, loaderArgs, this)

                loadSettings()
            } else {
                isNewPlayer = true
                val playerUri = newPlayer("", "", mFirebaseId)
                mPlayerId = playerUri.lastPathSegment.toInt()
            }

            // Set the main content of the app as the home fragment without adding it to the stack
            // so that hitting the back button doesn't result in an empty activity
            val transaction = supportFragmentManager.beginTransaction()
            mHomeFragment = HomeFragment.newInstance(mPlayerFirstName, mPlayerLastName)
            transaction.replace(R.id.main_activity_content, mHomeFragment)
            transaction.commit()

            if(isNewPlayer) {
                showNewDialog(EditProfile.newInstance(mPlayerFirstName, mPlayerLastName))
            }
        }
    }

    override fun onStop() {
        super.onStop()
        saveSettings()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.actions, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean = when(item?.itemId) {
            // TODO open PreferenceActivity since apparently it is its own special thing
            R.id.action_settings -> {
                LOG.info("selected the settings action")
                true
            }
            R.id.action_sign_out -> {
                signOut()
                true
            }
            else -> super.onOptionsItemSelected(item)
    }

    private fun showNewFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()

        transaction.replace(R.id.main_activity_content, fragment)
        transaction.addToBackStack(null)

        // Commit the transaction
        transaction.commit()
    }


    /**
     * Add a new season to the sql database. Seasons are annoying because they can be either all in
     * 1 calendar year or 2. So creating an entry for every type of season when they're needed
     */
    private fun newSeason(startYear: Int, endYear: Int): Uri {
        val newValues = ContentValues()
        newValues.put(StatsContract.SeasonList.COLUMN_NAME_START_YEAR, startYear)
        newValues.put(StatsContract.SeasonList.COLUMN_NAME_END_YEAR, endYear)

        return contentResolver.insert(
                StatsContract.SeasonList.CONTENT_URI,
                newValues
        )
    }

    /**
     * Add a new team to the sql database
     */
    private fun newTeam(team: String, city: String, state: String, seasonId: Int): Uri {
        // Create the team
        val teamValues = ContentValues()
        teamValues.put(StatsContract.TeamInfo.COLUMN_NAME_TEAM_NAME, team)
        teamValues.put(StatsContract.TeamInfo.COLUMN_NAME_CITY, city)
        teamValues.put(StatsContract.TeamInfo.COLUMN_NAME_STATE, state)

        val uri = contentResolver.insert(
                StatsContract.TeamInfo.CONTENT_URI,
                teamValues
        )
        val teamId = uri.lastPathSegment.toInt()


        // Add the person creating the team to the roster as a captain
        addToRoster(teamId.toLong(), seasonId.toLong(), mPlayerId.toLong(), true)

        return uri
    }

    /**
     * Add a new player to the sql database
     */
    private fun newPlayer(firstName: String, lastName: String, uid: String? = ""): Uri {
        val newValues = ContentValues()
        newValues.put(StatsContract.PlayerInfo.COLUMN_NAME_FIRST_NAME, firstName)
        newValues.put(StatsContract.PlayerInfo.COLUMN_NAME_LAST_NAME, lastName)
        newValues.put(StatsContract.PlayerInfo.COLUMN_NAME_FIREBASE_UID, uid)

        return contentResolver.insert(
                StatsContract.PlayerInfo.CONTENT_URI,
                newValues
        )
    }

    private fun addToRoster(teamId: Long, seasonId: Long, playerId: Long, isCaptain: Boolean) {
        val rosterValues = ContentValues()
        rosterValues.put(StatsContract.Roster.COLUMN_NAME_TEAM_ID, teamId)
        rosterValues.put(StatsContract.Roster.COLUMN_NAME_SEASON_ID, seasonId)
        rosterValues.put(StatsContract.Roster.COLUMN_NAME_PLAYER_ID, playerId)
        rosterValues.put(StatsContract.Roster.COLUMN_NAME_CAPTAIN, isCaptain)

        contentResolver.insert(
                StatsContract.Roster.CONTENT_URI,
                rosterValues
        )
    }

    /**
     * listener to show a dialog when a fragment widget is clicked
     */
    override fun showNewDialog(dialog: DialogFragment) {
        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        val ft = supportFragmentManager.beginTransaction()
        val prev = supportFragmentManager.findFragmentByTag("dialog")
        if (prev != null) {
            ft.remove(prev)
        }
        ft.addToBackStack(null)

        // Create and show the dialog.
        dialog.show(ft, "dialog")
    }

    /**
     * interface function necessary for handling positive button clicks on custom dialog fragments
     * In General:
     *  Get info from dialog
     *  dismiss dialog
     *  perform action based on dialog info
     *
     * Probably a way to generalize these into "input dialogs" with the ability to specify the
     * entry fields and which ones are required, then perform the correct action based on their info
     */
    override fun onCreateClick(dialog: DialogFragment) {
        when(dialog) {
            is NewTeam -> {
                val teamName = dialog.teamName
                val city = dialog.city
                val state = dialog.state
                val seasonEnd = dialog.seasonEnd

                dialog.dismiss()
                // if the team name is NOT empty, create a new team
                if (teamName.isNotBlank() && city.isNotBlank() && state.isNotBlank()) {

                    // Check if there exists a season ID for the exact combination of years we need
                    val calendar: Calendar = Calendar.getInstance()
                    val startYear = calendar.get(Calendar.YEAR)
                    val endYear = startYear + seasonEnd!!
                    val selection = """${StatsContract.SeasonList.COLUMN_NAME_START_YEAR}=$startYear AND
                                       ${StatsContract.SeasonList.COLUMN_NAME_END_YEAR}= $endYear"""

                    val projection = arrayOf(StatsContract.SeasonList.COLUMN_NAME_SEASON_ID)
                    val cursor = contentResolver.query(StatsContract.SeasonList.CONTENT_URI, projection, selection,
                            null, null)

                    // if the season exists, use it's ID. Otherwise, create a new season
                    var seasonId =
                            if(cursor.moveToFirst())
                                cursor.getInt(cursor.getColumnIndex(StatsContract.SeasonList.COLUMN_NAME_SEASON_ID))
                            else
                                newSeason(startYear, endYear).lastPathSegment.toInt()

                    // Add the team to the database
                    val teamUri = newTeam(teamName, city, state, seasonId)

                    //showNewFragment(TeamPageFragment.newInstance(teamUri.lastPathSegment.toInt(), seasonId))
                }
            }

            is NewGame -> {
                val opponent = dialog.opponent

                dialog.dismiss()

                // showNewFragment(GameStateFragment.newInstance(opponent))
            }

            is AddPlayer -> {
                dialog.dismiss()
                val uri = newPlayer(dialog.firstName, dialog.lastName)
                val playerId = ContentUris.parseId(uri)
                addToRoster(dialog.teamId, dialog.seasonId, playerId, false)

            }

            is EditProfile -> {
                mPlayerFirstName = dialog.firstName
                mPlayerLastName = dialog.lastName

                dialog.dismiss()
                updatePlayerProfile()
            }
        }
    }

    /**
     * Quite possibly unnecessary
     */
    override fun onCancelClick(dialog: DialogFragment) {

        dialog.dismiss()
    }

    /**
     * For the loader callback interface
     */
    override fun onCreateLoader(id: Int, args: Bundle?): Loader<Cursor> {
        // At the moment we only have 1 loader so we can ignore the id value
        val selection = "${StatsContract.Roster.COLUMN_NAME_PLAYER_ID}=?"
        return CursorLoader(this,                               // Context
                StatsContract.Roster.CONTENT_TEAM_URI,                  // URI
                mRosterProjection,                                      // Projection
                selection,                                              // Selection
                args!!.getStringArray("playerid"),                 // Selection args
                StatsContract.TeamInfo.COLUMN_NAME_TEAM_NAME + " desc") // Sort
    }

    override fun onLoadFinished(loader: Loader<Cursor>?, data: Cursor?) {
        // get the current season of each team
        // TODO currently this data is not being passed to the team fragment
        // Should be passed to the team fragment so that you can select an old season or new season
        // to reset stats, roster, and such
        while(data!!.moveToNext()) {
            val teamId = data.getInt(data.getColumnIndex(StatsContract.Roster.COLUMN_NAME_TEAM_ID))
            if(!mTeamsAndSeasons.containsKey(teamId)) {
                mTeamsAndSeasons.put(teamId, ArrayList<Int>())
                mTeamList!!.addRow(arrayOf(
                        teamId,
                        data.getString(data.getColumnIndex(StatsContract.TeamInfo.COLUMN_NAME_TEAM_NAME)))
                )
            }
            mTeamsAndSeasons[teamId]!!.add(data.getColumnIndex(StatsContract.Roster.COLUMN_NAME_SEASON_ID))
        }

        mTeamAdapter.changeCursor(mTeamList)
    }

    override fun onLoaderReset(loader: Loader<Cursor>?) {
        mTeamAdapter.changeCursor(null)
    }

    private fun signOut() {
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        startActivity(LoginActivity.createIntent(this@MainActivity))
                        finish()
                    }
                }
    }

    /**
     * probably needed eventually
     */
    private fun loadSettings() {
        // val settings = getSharedPreferences("${mPlayerId}_settings", 0)

        // val silent = settings.getBoolean("silentMode", false)
    }

    /**
     * probably needed eventually
     */
    private fun saveSettings() {
        val settings = getSharedPreferences("${mPlayerId}_settings", 0)
        val editor = settings.edit()

        // editor.putBoolean("silentMode", mSilentMode)

        // Commit the edits!
        editor.apply()
    }

    private fun getPlayerProfile(cursor: Cursor) {
        mPlayerId = cursor.getInt(cursor.getColumnIndex(StatsContract.PlayerInfo.COLUMN_NAME_ID))
        mPlayerFirstName = cursor.getString(cursor.getColumnIndex(StatsContract.PlayerInfo.COLUMN_NAME_FIRST_NAME))
        mPlayerLastName = cursor.getString(cursor.getColumnIndex(StatsContract.PlayerInfo.COLUMN_NAME_LAST_NAME))

    }

    private fun updatePlayerProfile() {
        val updatedValues = ContentValues()
        updatedValues.put(StatsContract.PlayerInfo.COLUMN_NAME_FIRST_NAME, mPlayerFirstName)
        updatedValues.put(StatsContract.PlayerInfo.COLUMN_NAME_LAST_NAME, mPlayerLastName)
        contentResolver.update(
                StatsContract.PlayerInfo.CONTENT_URI.buildUpon().appendPath(mPlayerId.toString()).build(),
                updatedValues,
                null, null)
    }
}

package com.verboseoutput.upcall.dialog


import android.app.Activity
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import com.verboseoutput.upcall.R
import com.verboseoutput.upcall.logic.logger
import org.jetbrains.anko.find


class NewGame : DialogFragment() {
    companion object {
        private val TAG = "NewGame"
        val LOG = logger()
    }

    // initialize when first used
    private val mOpponent   by lazy { view?.find<EditText>(R.id.opponent) }

    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    interface NewGameDialogListener {
        fun onCreateClick(dialog: DialogFragment)
        fun onCancelClick(dialog: DialogFragment)
    }

    // Use this instance of the interface to deliver action events
    internal var mListener: NewGameDialogListener? = null

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = activity as NewGameDialogListener?
        } catch (e: ClassCastException) {
            // The activity doesn't implement the interface, throw exception
            throw ClassCastException(activity!!.toString() + " must implement NewGameDialogListener")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view =  inflater!!.inflate(R.layout.dialog_new_game, container, false)

        val createButton = view?.find<Button>(R.id.createButton)
        val cancelButton = view?.find<Button>(R.id.cancelButton)

        createButton?.setOnClickListener {
            mListener?.onCreateClick(this@NewGame)
        }
        cancelButton?.setOnClickListener {
            mListener?.onCancelClick(this@NewGame)
        }

        return view
    }

    val opponent: String
        get() = mOpponent?.text.toString()

}

package com.verboseoutput.upcall.dialog

import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import org.jetbrains.anko.find
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import com.verboseoutput.upcall.R


class EditProfile : DialogFragment() {
    private var mFirstName: String? = null
    private var mLastName: String? = null
    private var mFirstNameEdit: EditText? = null
    private var mLastNameEdit: EditText? = null

    val firstName get() = mFirstNameEdit?.text.toString()
    val lastName get() = mLastNameEdit?.text.toString()

    private var mListener: EditProfileDialogListener? = null

    interface EditProfileDialogListener {
        fun onCreateClick(dialog: DialogFragment)
        fun onCancelClick(dialog: DialogFragment)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mFirstName = arguments.getString(ARG_FIRST_NAME)
            mLastName = arguments.getString(ARG_LAST_NAME)
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater!!.inflate(R.layout.dialog_edit_profile, container, false)

        mFirstNameEdit = view?.find(R.id.first_name_edit)
        mLastNameEdit = view?.find(R.id.last_name_edit)
        mFirstNameEdit?.setText(mFirstName)
        mLastNameEdit?.setText(mLastName)

        val updateButton = view.find<Button>(R.id.update_button)
        val cancelButton = view.find<Button>(R.id.cancel_button)

        updateButton.setOnClickListener {
            mListener?.onCreateClick(this@EditProfile)
        }

        cancelButton.setOnClickListener {
            mListener?.onCancelClick(this@EditProfile)
        }

        return view
    }



    override fun onAttach(context: Context?) {
        super.onAttach(context)
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = activity as EditProfileDialogListener?
        } catch (e: ClassCastException) {
            // The activity doesn't implement the interface, throw exception
            throw ClassCastException(activity!!.toString() + " must implement EditProfileDialogListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }




    companion object {
        private val ARG_FIRST_NAME = "firstName"
        private val ARG_LAST_NAME = "lastName"

        /**
         * @return A new instance of fragment EditProfile.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(firstName: String, lastName: String): EditProfile {
            val fragment = EditProfile()
            val args = Bundle()
            args.putString(ARG_FIRST_NAME, firstName)
            args.putString(ARG_LAST_NAME, lastName)
            fragment.arguments = args
            return fragment
        }
    }
}

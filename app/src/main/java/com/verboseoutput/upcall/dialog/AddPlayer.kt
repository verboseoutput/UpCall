package com.verboseoutput.upcall.dialog


import android.app.Activity
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import com.verboseoutput.upcall.R
import com.verboseoutput.upcall.logic.logger
import org.jetbrains.anko.find

/**
 * Add a player to a team roster
 */
class AddPlayer : DialogFragment() {
    // initialize when first used
    private val mFirstName   by lazy { view?.find<EditText>(R.id.name_first) }
    private val mLastName   by lazy { view?.find<EditText>(R.id.name_last) }
    private var mTeamId: Long = -1
    private var mSeasonId: Long = -1
    // public accessors
    val teamId get() = mTeamId
    val seasonId get() = mSeasonId

    val firstName: String
        get() = mFirstName?.text.toString()

    val lastName: String
        get() = mLastName?.text.toString()


    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    interface NewPlayerDialogListener {
        fun onCreateClick(dialog: DialogFragment)
        fun onCancelClick(dialog: DialogFragment)
    }

    // Use this instance of the interface to deliver action events
    private var mListener: NewPlayerDialogListener? = null

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = activity as NewPlayerDialogListener?
        } catch (e: ClassCastException) {
            // The activity doesn't implement the interface, throw exception
            throw ClassCastException(activity!!.toString() + " must implement NewPlayerDialogListener")
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(arguments != null) {
            mTeamId = arguments.getLong(ARG_TEAM_ID)
            mSeasonId = arguments.getLong(ARG_SEASON_ID)
        }
    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view =  inflater?.inflate(R.layout.dialog_new_player, container, false)

        val createButton = view?.find<Button>(R.id.newPlayerCreateButton)
        val cancelButton = view?.find<Button>(R.id.newPlayerCancelButton)

        createButton?.setOnClickListener {
            mListener?.onCreateClick(this@AddPlayer)
        }
        cancelButton?.setOnClickListener {
            mListener?.onCancelClick(this@AddPlayer)
        }

        return view
    }

    companion object {
        private val ARG_TEAM_ID = "teamId"
        private val ARG_SEASON_ID = "seasonId"

        val LOG = logger()
        fun newInstance(teamId: Long, seasonId: Long): AddPlayer {
            val fragment = AddPlayer()
            val args = Bundle()
            args.putLong(ARG_TEAM_ID, teamId)
            args.putLong(ARG_SEASON_ID, seasonId)
            fragment.arguments = args
            return fragment
        }
    }

}
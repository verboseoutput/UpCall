package com.verboseoutput.upcall.logic

import android.content.ContentResolver
import android.net.Uri

object StatsContract {

    /**
     * URI building blocks
     */
    const val CONTENT_AUTHORITY: String = "com.verboseoutput.upcall"
    val BASE_CONTENT_URI: Uri = Uri.parse("content://" + CONTENT_AUTHORITY)

    const  val PATH_TEAM_INFO = "team_info"
    const  val PATH_SEASON = "season_list"
    const  val PATH_GAME_STAT = "game_stat"
    const  val PATH_PLAYER_INFO = "player_info"
    const  val PATH_PLAYER_STAT = "player_stat"
    const  val PATH_ROSTER = "roster"
    const  val PATH_ROSTER_PLAYER = "roster_player"
    const  val PATH_ROSTER_TEAM = "roster_team"


    class TeamInfo private constructor() {
        companion object {
            /**
             * MIME type for lists of entries and individual entries
             */
            const val CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.upcall.team_info"
            const val CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.upcall.team_info"

            /**
             * Fully qualified URI for "team_list" resources.
             */
            val CONTENT_URI: Uri = BASE_CONTENT_URI.buildUpon().appendPath(PATH_TEAM_INFO).build()

            const val TABLE_NAME = "teams_tbl"
            const val COLUMN_NAME_TEAM_ID = "_id"
            const val COLUMN_NAME_TEAM_NAME = "team_name"
            const val COLUMN_NAME_CITY = "team_city"
            const val COLUMN_NAME_STATE = "team_state"
        }
    }

    class SeasonList private constructor() {
        companion object {
            /**
             * MIME type for lists of entries and individual entries
             */
            val CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.upcall.season"
            val CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.upcall.season"

            val CONTENT_URI: Uri = BASE_CONTENT_URI.buildUpon().appendPath(PATH_SEASON).build()

            val TABLE_NAME = "season_tbl"
            val COLUMN_NAME_SEASON_ID = "_id"
            val COLUMN_NAME_START_YEAR = "start_year"
            val COLUMN_NAME_END_YEAR = "end_year"

        }
    }

    class GameStat private constructor() {
        companion object {
            /**
             * MIME type for lists of entries and individual entries
             */
            const val CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.upcall.game_stat"
            const val CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.upcall.game_stat"

            /**
             * Fully qualified URI for "team_list" resources.
             */
            val CONTENT_URI: Uri = BASE_CONTENT_URI.buildUpon().appendPath(PATH_GAME_STAT).build()

            val TABLE_NAME = "game_stats_tbl"
            val COLUMN_NAME_TEAM_ID = "team_id"// Foreign key team_id from team_tbl
            val COLUMN_NAME_SEASON_ID = "season_id" // Foreign key season_id season_tbl
            val COLUMN_NAME_GAME_ID = "_id"
            val COLUMN_NAME_TOURNAMENT = "tournament"
            val COLUMN_NAME_DATETIME = "datetime"
            val COLUMN_NAME_OPPONENT_ID = "opponent_id"
            val COLUMN_NAME_POINTS_FOR = "points_for"
            val COLUMN_NAME_POINTS_AGAINST = "points_against"
        }
    }

    class PlayerInfo private constructor() {
        companion object {
            /**
             * MIME type for lists of entries and individual entries
             */
            val CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.upcall.player_info"
            val CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.upcall.player_info"

            /**
             * Fully qualified URI for "team_list" resources.
             */
            val CONTENT_URI: Uri = BASE_CONTENT_URI.buildUpon().appendPath(PATH_PLAYER_INFO).build()

            val TABLE_NAME = "player_info_tbl"
            val COLUMN_NAME_ID = "_id"
            val COLUMN_NAME_FIREBASE_UID = "uid"
            val COLUMN_NAME_FIRST_NAME = "first_name"
            val COLUMN_NAME_LAST_NAME = "last_name"
        }
    }

    class PlayerStat private constructor() {
        companion object {
            /**
             * MIME type for lists of entries and individual entries
             */
            val CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.upcall.player_stat"
            val CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.upcall.player_stat"

            /**
             * Fully qualified URI for "team_list" resources.
             */
            val CONTENT_URI: Uri = BASE_CONTENT_URI.buildUpon().appendPath(PATH_PLAYER_STAT).build()

            val TABLE_NAME = "player_stats_tbl"
            val COLUMN_NAME_GAME_ID = "game_id"
            val COLUMN_NAME_PLAYER_ID = "player_id"
            val COLUMN_NAME_O_POINTS_PLAYED = "o_points_played"
            val COLUMN_NAME_D_POINTS_PLAYED = "d_points_played"
            val COLUMN_NAME_POINTS_SCORED = "points_scored"
            val COLUMN_NAME_ASSISTS = "assists"
            val COLUMN_NAME_TOUCHES = "touches"
            val COLUMN_NAME_DS = "ds"
            val COLUMN_NAME_BLOCKS = "blocks"
            val COLUMN_NAME_DROPS = "drops"
            val COLUMN_NAME_THROW_AWAYS = "throw_aways"
        }
    }

    class Roster private constructor() {
        companion object {
            /**
             * MIME type for lists of entries and individual entries
             */
            val CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.upcall.roster"

            /**
             * Fully qualified URI for "roster" resources.
             */
            val CONTENT_URI: Uri = BASE_CONTENT_URI.buildUpon().appendPath(PATH_ROSTER).build()
            val CONTENT_PLAYER_URI: Uri = CONTENT_URI.buildUpon().appendPath(PATH_ROSTER_PLAYER).build()
            val CONTENT_TEAM_URI: Uri = CONTENT_URI.buildUpon().appendPath(PATH_ROSTER_TEAM).build()

            val TABLE_NAME = "roster_tbl"
            val COLUMN_NAME_TEAM_ID = "team_id"
            val COLUMN_NAME_SEASON_ID = "season_id"
            val COLUMN_NAME_PLAYER_ID = "player_id"
            val COLUMN_NAME_CAPTAIN = "captain"
        }
    }


}

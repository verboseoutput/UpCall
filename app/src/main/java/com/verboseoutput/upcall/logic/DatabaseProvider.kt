package com.verboseoutput.upcall.logic

import android.content.ContentProvider
import android.content.ContentValues
import android.content.Context
import android.content.UriMatcher
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.net.Uri
import android.provider.BaseColumns

class DatabaseProvider : ContentProvider() {
    lateinit private var mDatabaseHelper: StatsDatabaseHelper

    override fun onCreate(): Boolean {
        mDatabaseHelper = StatsDatabaseHelper(context)
        return true
    }

    override fun getType(uri: Uri): String? {
        val match = sUriMatcher.match(uri)
        when (match) {
            ROUTE_TEAM_INFO -> return StatsContract.TeamInfo.CONTENT_TYPE
            ROUTE_TEAM_INFO_ID -> return StatsContract.TeamInfo.CONTENT_ITEM_TYPE
            ROUTE_SEASON_LIST -> return StatsContract.SeasonList.CONTENT_TYPE
            ROUTE_SEASON_LIST_ID -> return StatsContract.SeasonList.CONTENT_ITEM_TYPE
            ROUTE_GAME_STAT -> return StatsContract.GameStat.CONTENT_TYPE
            ROUTE_GAME_STAT_ID -> return StatsContract.GameStat.CONTENT_ITEM_TYPE
            ROUTE_PLAYER_INFO -> return StatsContract.PlayerInfo.CONTENT_TYPE
            ROUTE_PLAYER_INFO_ID -> return StatsContract.PlayerInfo.CONTENT_ITEM_TYPE
            ROUTE_PLAYER_STAT -> return StatsContract.PlayerStat.CONTENT_TYPE
            ROUTE_PLAYER_STAT_ID -> return StatsContract.PlayerStat.CONTENT_ITEM_TYPE
            ROUTE_ROSTER -> return StatsContract.Roster.CONTENT_TYPE
            // ROUTE_ROSTER_ID -> return StatsContract.Roster.CONTENT_ITEM_TYPE         NOT A THING
            else -> throw UnsupportedOperationException("Unknown uri: " + uri)
        }
    }

    /**
     * Perform a database query by URI.
     *
     * Currently supports returning all entries (/entries) and individual entries by ID
     * (/entries/{ID}).
     */
    override fun query(uri: Uri, projection: Array<String>, selection: String?, selectionArgs: Array<String>?,
                       sortOrder: String?): Cursor {

        val builder = SelectionBuilder()
        val uriMatch = sUriMatcher.match(uri)
        val id: String

        when (uriMatch) {
            ROUTE_TEAM_INFO_ID -> {
                builder.table(StatsContract.TeamInfo.TABLE_NAME)
                id = uri.lastPathSegment
                builder.where(StatsContract.TeamInfo.COLUMN_NAME_TEAM_ID + "=?", arrayOf(id))
            }
            ROUTE_TEAM_INFO -> {
                builder.table(StatsContract.TeamInfo.TABLE_NAME)
            }
            ROUTE_SEASON_LIST_ID -> {
                builder.table(StatsContract.SeasonList.TABLE_NAME)
                id = uri.lastPathSegment
                builder.where(StatsContract.SeasonList.COLUMN_NAME_SEASON_ID + "=?", arrayOf(id))
            }
            ROUTE_SEASON_LIST -> {
                builder.table(StatsContract.SeasonList.TABLE_NAME)
            }
            ROUTE_GAME_STAT_ID -> {
                builder.table(StatsContract.GameStat.TABLE_NAME)
                id = uri.lastPathSegment
                builder.where(StatsContract.GameStat.COLUMN_NAME_GAME_ID + "=?", arrayOf(id))
            }
            ROUTE_GAME_STAT -> {
                builder.table(StatsContract.GameStat.TABLE_NAME)
            }

            ROUTE_PLAYER_INFO_ID -> {
                builder.table(StatsContract.PlayerInfo.TABLE_NAME)
                id = uri.lastPathSegment
                builder.where(StatsContract.PlayerInfo.COLUMN_NAME_ID + "=?", arrayOf(id))
            }
            ROUTE_PLAYER_INFO -> {
                builder.table(StatsContract.PlayerInfo.TABLE_NAME)
            }
            ROUTE_PLAYER_STAT_ID -> {
                builder.table(StatsContract.PlayerStat.TABLE_NAME)
                val ids = uri.pathSegments
                builder.where(StatsContract.PlayerStat.COLUMN_NAME_GAME_ID + "=?",
                        ids.subList(ids.size-2, ids.size-1).toTypedArray())
                builder.where(StatsContract.PlayerStat.COLUMN_NAME_PLAYER_ID + "=?",
                        ids.subList(ids.size-1, ids.size).toTypedArray())
            }
            ROUTE_PLAYER_STAT -> {
                builder.table(StatsContract.PlayerStat.TABLE_NAME)
            }
            ROUTE_ROSTER_PLAYER_INFO -> {
                val ROSTER_INNER_JOIN_PLAYER_INFO =
                        """
                        ${StatsContract.Roster.TABLE_NAME} INNER JOIN
                        ${StatsContract.PlayerInfo.TABLE_NAME} ON
                        ${StatsContract.Roster.COLUMN_NAME_PLAYER_ID}=${StatsContract.PlayerInfo.COLUMN_NAME_ID}
                        """
                builder.table(ROSTER_INNER_JOIN_PLAYER_INFO)
                builder.mapToTable(StatsContract.PlayerInfo.COLUMN_NAME_ID,
                                   StatsContract.PlayerInfo.TABLE_NAME)
                builder.map(BaseColumns._ID, StatsContract.Roster.COLUMN_NAME_PLAYER_ID)
            }
            ROUTE_ROSTER_TEAM_INFO -> {
                val ROSTER_INNER_JOIN_TEAM_INFO =
                        """
                        ${StatsContract.Roster.TABLE_NAME} INNER JOIN
                        ${StatsContract.TeamInfo.TABLE_NAME} ON
                        ${StatsContract.Roster.COLUMN_NAME_TEAM_ID}=${StatsContract.TeamInfo.COLUMN_NAME_TEAM_ID}
                        """
                builder.table(ROSTER_INNER_JOIN_TEAM_INFO)
                builder.mapToTable(StatsContract.TeamInfo.COLUMN_NAME_TEAM_ID,
                        StatsContract.TeamInfo.TABLE_NAME)
                builder.map(BaseColumns._ID, StatsContract.Roster.COLUMN_NAME_TEAM_ID)
            }

            else -> throw UnsupportedOperationException("Unknown uri: " + uri)
        }
        // where can't have a null passed to it anymore
        selection?.let {  builder.where(selection, selectionArgs) }

        val db = mDatabaseHelper.readableDatabase
        val cursor = builder.query(db, projection, sortOrder)
        // Note: Notification URI must be manually set here for loaders to correctly
        // register ContentObservers.
        if(context != null) cursor.setNotificationUri(context.contentResolver, uri)
        return cursor
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        val db = mDatabaseHelper.writableDatabase!!
        val match = sUriMatcher.match(uri)
        val result: Uri
        val id: Long
        when (match) {
            ROUTE_TEAM_INFO -> {
                id = db.insertOrThrow(StatsContract.TeamInfo.TABLE_NAME, null, values)
                result = Uri.parse(StatsContract.TeamInfo.CONTENT_URI.toString() + "/" + id)
            }
            ROUTE_SEASON_LIST -> {
                id = db.insertOrThrow(StatsContract.SeasonList.TABLE_NAME, null, values)
                result = Uri.parse(StatsContract.SeasonList.CONTENT_URI.toString() + "/" + id)
            }
            ROUTE_GAME_STAT -> {
                id = db.insertOrThrow(StatsContract.GameStat.TABLE_NAME, null, values)
                result = Uri.parse(StatsContract.GameStat.CONTENT_URI.toString() + "/" + id)
            }
            ROUTE_PLAYER_INFO -> {
                id = db.insertOrThrow(StatsContract.PlayerInfo.TABLE_NAME, null, values)
                result = Uri.parse(StatsContract.PlayerInfo.CONTENT_URI.toString() + "/" + id)
            }
            ROUTE_PLAYER_STAT -> {
                id = db.insertOrThrow(StatsContract.PlayerStat.TABLE_NAME, null, values)
                result = Uri.parse(StatsContract.PlayerStat.CONTENT_URI.toString() + "/" + id)
            }
            ROUTE_ROSTER -> {
                id = db.insertOrThrow(StatsContract.Roster.TABLE_NAME, null, values)
                result = Uri.parse(StatsContract.Roster.CONTENT_URI.toString() + "/" + id)
            }
            ROUTE_TEAM_INFO_ID, ROUTE_SEASON_LIST_ID, ROUTE_GAME_STAT_ID, ROUTE_PLAYER_INFO_ID,
            ROUTE_PLAYER_STAT_ID -> throw UnsupportedOperationException("Insert not supported on URI: " + uri)
            else -> throw UnsupportedOperationException("Unknown uri: " + uri)
        }
        // Send broadcast to registered ContentObservers, to refresh UI.
        context!!.contentResolver.notifyChange(uri, null, false)
        return result
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
        val builder = SelectionBuilder()
        val db = mDatabaseHelper.writableDatabase
        val match = sUriMatcher.match(uri)
        val id: String
        val count: Int
        when (match) {
            ROUTE_TEAM_INFO ->  {
                builder.table(StatsContract.TeamInfo.TABLE_NAME)
                selection?.let {  builder.where(selection, selectionArgs) }
                count = builder.delete(db)
            }
            ROUTE_TEAM_INFO_ID -> {
                id = uri.lastPathSegment
                builder.table(StatsContract.TeamInfo.TABLE_NAME).where(StatsContract.TeamInfo.COLUMN_NAME_TEAM_ID + "=?", arrayOf(id))
                selection?.let {  builder.where(selection, selectionArgs) }
                count = builder.delete(db)
            }
            ROUTE_SEASON_LIST -> {
                builder.table(StatsContract.SeasonList.TABLE_NAME)
                selection?.let {  builder.where(selection, selectionArgs) }
                count = builder.delete(db)
            }
            ROUTE_SEASON_LIST_ID -> {
                id = uri.lastPathSegment
                builder.table(StatsContract.SeasonList.TABLE_NAME).where(StatsContract.SeasonList.COLUMN_NAME_SEASON_ID + "=?", arrayOf(id))
                selection?.let {  builder.where(selection, selectionArgs) }
                count = builder.delete(db)
            }
            ROUTE_GAME_STAT -> {
                builder.table(StatsContract.GameStat.TABLE_NAME)
                selection?.let {  builder.where(selection, selectionArgs) }
                count = builder.delete(db)
            }
            ROUTE_GAME_STAT_ID -> {
                id = uri.lastPathSegment
                builder.table(StatsContract.GameStat.TABLE_NAME).where(StatsContract.GameStat.COLUMN_NAME_GAME_ID + "=?", arrayOf(id))
                selection?.let {  builder.where(selection, selectionArgs) }
                count = builder.delete(db)
            }
            ROUTE_PLAYER_INFO -> {
                builder.table(StatsContract.PlayerInfo.TABLE_NAME)
                selection?.let {  builder.where(selection, selectionArgs) }
                count = builder.delete(db)
            }
            ROUTE_PLAYER_INFO_ID -> {
                id = uri.lastPathSegment
                builder.table(StatsContract.PlayerInfo.TABLE_NAME).where(StatsContract.PlayerInfo.COLUMN_NAME_ID + "=?", arrayOf(id))
                selection?.let {  builder.where(selection, selectionArgs) }
                count = builder.delete(db)
            }
            ROUTE_PLAYER_STAT -> {
                builder.table(StatsContract.PlayerStat.TABLE_NAME)
                selection?.let {  builder.where(selection, selectionArgs) }
                count = builder.delete(db)
            }
            ROUTE_PLAYER_STAT_ID -> {
                val ids = uri.pathSegments
                builder.table(StatsContract.PlayerStat.TABLE_NAME)
                builder.where(StatsContract.PlayerStat.COLUMN_NAME_GAME_ID + "=?",
                        ids.subList(ids.size-2, ids.size-1).toTypedArray())
                builder.where(StatsContract.PlayerStat.COLUMN_NAME_PLAYER_ID + "=?",
                        ids.subList(ids.size-1, ids.size).toTypedArray())
                selection?.let {  builder.where(selection, selectionArgs) }
                count = builder.delete(db)
            }
            ROUTE_ROSTER -> {
                builder.table(StatsContract.Roster.TABLE_NAME)
                selection?.let {  builder.where(selection, selectionArgs) }
                count = builder.delete(db)
            }
            else -> throw UnsupportedOperationException("Unknown uri: " + uri)
        }
        // Send broadcast to registered ContentObservers, to refresh UI.
        val ctx = context!!
        ctx.contentResolver.notifyChange(uri, null, false)
        return count
    }

    override fun update(uri: Uri, values: ContentValues, selection: String?,
                        selectionArgs: Array<String>?): Int {
        val builder = SelectionBuilder()
        val db = mDatabaseHelper.writableDatabase
        val match = sUriMatcher.match(uri)
        val id: String
        val count: Int
        when (match) {
            ROUTE_TEAM_INFO -> {
                builder.table(StatsContract.TeamInfo.TABLE_NAME)
                selection?.let {  builder.where(selection, selectionArgs) }
                count = builder.update(db, values)
            }
            ROUTE_TEAM_INFO_ID -> {
                id = uri.lastPathSegment
                builder.table(StatsContract.TeamInfo.TABLE_NAME).where(StatsContract.TeamInfo.COLUMN_NAME_TEAM_ID + "=?", arrayOf(id))
                selection?.let {  builder.where(selection, selectionArgs) }
                count = builder.update(db, values)
            }
            ROUTE_SEASON_LIST -> {
                builder.table(StatsContract.SeasonList.TABLE_NAME)
                selection?.let {  builder.where(selection, selectionArgs) }
                count = builder.update(db, values)
            }
            ROUTE_SEASON_LIST_ID -> {
                id = uri.lastPathSegment
                builder.table(StatsContract.SeasonList.TABLE_NAME).where(StatsContract.SeasonList.COLUMN_NAME_SEASON_ID + "=?", arrayOf(id))
                selection?.let {  builder.where(selection, selectionArgs) }
                count = builder.update(db, values)
            }
            ROUTE_GAME_STAT -> {
                builder.table(StatsContract.GameStat.TABLE_NAME)
                selection?.let {  builder.where(selection, selectionArgs) }
                count = builder.update(db, values)
            }
            ROUTE_GAME_STAT_ID -> {
                id = uri.lastPathSegment
                builder.table(StatsContract.GameStat.TABLE_NAME).where(StatsContract.GameStat.COLUMN_NAME_GAME_ID + "=?", arrayOf(id))
                selection?.let {  builder.where(selection, selectionArgs) }
                count = builder.update(db, values)
            }
            ROUTE_PLAYER_INFO -> {
                builder.table(StatsContract.PlayerInfo.TABLE_NAME)
                selection?.let {  builder.where(selection, selectionArgs) }
                count = builder.update(db, values)
            }
            ROUTE_PLAYER_INFO_ID -> {
                id = uri.lastPathSegment
                builder.table(StatsContract.PlayerInfo.TABLE_NAME).where(StatsContract.PlayerInfo.COLUMN_NAME_ID + "=?", arrayOf(id))
                selection?.let {  builder.where(selection, selectionArgs) }
                count = builder.update(db, values)
            }
            ROUTE_PLAYER_STAT -> {
                builder.table(StatsContract.PlayerStat.TABLE_NAME)
                selection?.let {  builder.where(selection, selectionArgs) }
                count = builder.update(db, values)
            }
            ROUTE_PLAYER_STAT_ID -> {
                val ids = uri.pathSegments
                builder.table(StatsContract.PlayerStat.TABLE_NAME)
                builder.where(StatsContract.PlayerStat.COLUMN_NAME_GAME_ID + "=?",
                        ids.subList(ids.size-2, ids.size-1).toTypedArray())
                builder.where(StatsContract.PlayerStat.COLUMN_NAME_PLAYER_ID + "=?",
                        ids.subList(ids.size-1, ids.size).toTypedArray())
                selection?.let {  builder.where(selection, selectionArgs) }
                count = builder.update(db, values)
            }
            ROUTE_ROSTER -> {
                builder.table(StatsContract.GameStat.TABLE_NAME)
                selection?.let {  builder.where(selection, selectionArgs) }
                count = builder.update(db, values)
            }
            else -> throw UnsupportedOperationException("Unknown uri: " + uri)
        }
        val ctx = context!!
        ctx.contentResolver.notifyChange(uri, null, false)
        return count
    }

    /**
     * should not be accessed outside of the content provider
     */
    private class StatsDatabaseHelper(context: Context) :
            SQLiteOpenHelper(context, DATABASE_NAME, null,
                                DatabaseProvider.StatsDatabaseHelper.DATABASE_VERSION) {

        /*
         * only called if Stats.db did not exist before
         */
        override fun onCreate(db: SQLiteDatabase) {
            db.execSQL(SQL_CREATE_TEAM_INFO_TABLE)
            db.execSQL(SQL_CREATE_SEASON_LIST_TABLE)
            db.execSQL(SQL_CREATE_GAME_STAT_TABLE)
            db.execSQL(SQL_CREATE_PLAYER_INFO_TABLE)
            db.execSQL(SQL_CREATE_PLAYER_STAT_TABLE)
            db.execSQL(SQL_CREATE_ROSTER_TABLE)

            db.execSQL(SQL_CREATE_GAME_STAT_INDEX)
        }

        override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
            /*
         * going to have to come up with a decent upgrade strategy so that we don't
         * destroy data
         */
        }

        override fun onConfigure(db: SQLiteDatabase) {
            super.onConfigure(db)
            if(!db.isReadOnly) {
                // Make sure foreign key support is enabled
                db.execSQL("PRAGMA foreign_keys=ON;")
            }
        }

        companion object {
            // If you change the database schema, you must increment the database version.
            internal val DATABASE_VERSION = 1
            internal val DATABASE_NAME = "Stats.db"

            // MUST use a cursor wrapper to supply a _id to the cursor adapter
            val SQL_CREATE_TEAM_INFO_TABLE =
                    """
                        CREATE TABLE ${StatsContract.TeamInfo.TABLE_NAME} (
                            ${StatsContract.TeamInfo.COLUMN_NAME_TEAM_ID}   INTEGER PRIMARY KEY,
                            ${StatsContract.TeamInfo.COLUMN_NAME_STATE}     TEXT,
                            ${StatsContract.TeamInfo.COLUMN_NAME_CITY}      TEXT,
                            ${StatsContract.TeamInfo.COLUMN_NAME_TEAM_NAME} TEXT
                        );
                    """

            val SQL_CREATE_SEASON_LIST_TABLE =
                    """
                        CREATE TABLE ${StatsContract.SeasonList.TABLE_NAME} (
                            ${StatsContract.SeasonList.COLUMN_NAME_SEASON_ID}   INTEGER PRIMARY KEY,
                            ${StatsContract.SeasonList.COLUMN_NAME_START_YEAR}  INTEGER NOT NULL,
                            ${StatsContract.SeasonList.COLUMN_NAME_END_YEAR}    INTEGER NOT NULL
                        );
                    """

            val SQL_CREATE_GAME_STAT_TABLE =
                    """
                        CREATE TABLE ${StatsContract.GameStat.TABLE_NAME} (
                            ${StatsContract.GameStat.COLUMN_NAME_GAME_ID} INTEGER PRIMARY KEY,
                            ${StatsContract.GameStat.COLUMN_NAME_TEAM_ID} INTEGER REFERENCES ${StatsContract.TeamInfo.TABLE_NAME},
                            ${StatsContract.GameStat.COLUMN_NAME_SEASON_ID} INTEGER REFERENCES ${StatsContract.SeasonList.TABLE_NAME},
                            ${StatsContract.GameStat.COLUMN_NAME_TOURNAMENT} TEXT,
                            ${StatsContract.GameStat.COLUMN_NAME_DATETIME} DATETIME,
                            ${StatsContract.GameStat.COLUMN_NAME_OPPONENT_ID} INTEGER,
                            ${StatsContract.GameStat.COLUMN_NAME_POINTS_FOR} INTEGER,
                            ${StatsContract.GameStat.COLUMN_NAME_POINTS_AGAINST} INTEGER
                        );
                    """

            // Index on the foreign keys, team id comes first because that eliminates more of the table than season does
            val SQL_CREATE_GAME_STAT_INDEX =
                    """
                        CREATE INDEX game_state_index ON ${StatsContract.GameStat.TABLE_NAME}(
                            ${StatsContract.GameStat.COLUMN_NAME_TEAM_ID},
                            ${StatsContract.GameStat.COLUMN_NAME_SEASON_ID});
                    """

            val SQL_CREATE_PLAYER_INFO_TABLE =
                    """
                        CREATE TABLE ${StatsContract.PlayerInfo.TABLE_NAME} (
                            ${StatsContract.PlayerInfo.COLUMN_NAME_ID} INTEGER PRIMARY KEY,
                            ${StatsContract.PlayerInfo.COLUMN_NAME_FIREBASE_UID} TEXT,
                            ${StatsContract.PlayerInfo.COLUMN_NAME_FIRST_NAME} TEXT,
                            ${StatsContract.PlayerInfo.COLUMN_NAME_LAST_NAME} TEXT
                        );
                    """

            val SQL_CREATE_PLAYER_STAT_TABLE =
                    """
                        CREATE TABLE ${StatsContract.PlayerStat.TABLE_NAME} (
                            ${StatsContract.PlayerStat.COLUMN_NAME_GAME_ID} INTEGER NOT NULL REFERENCES ${StatsContract.GameStat.TABLE_NAME},
                            ${StatsContract.PlayerStat.COLUMN_NAME_PLAYER_ID} INTEGER NOT NULL REFERENCES ${StatsContract.PlayerInfo.TABLE_NAME},
                            ${StatsContract.PlayerStat.COLUMN_NAME_O_POINTS_PLAYED} INTEGER,
                            ${StatsContract.PlayerStat.COLUMN_NAME_D_POINTS_PLAYED} INTEGER,
                            ${StatsContract.PlayerStat.COLUMN_NAME_POINTS_SCORED} INTEGER,
                            ${StatsContract.PlayerStat.COLUMN_NAME_ASSISTS} INTEGER,
                            ${StatsContract.PlayerStat.COLUMN_NAME_TOUCHES} INTEGER,
                            ${StatsContract.PlayerStat.COLUMN_NAME_DS} INTEGER,
                            ${StatsContract.PlayerStat.COLUMN_NAME_BLOCKS} INTEGER,
                            ${StatsContract.PlayerStat.COLUMN_NAME_DROPS} INTEGER,
                            ${StatsContract.PlayerStat.COLUMN_NAME_THROW_AWAYS} INTEGER,
                            PRIMARY KEY (${StatsContract.PlayerStat.COLUMN_NAME_GAME_ID},
                                         ${StatsContract.PlayerStat.COLUMN_NAME_PLAYER_ID})
                        ) WITHOUT ROWID;
                    """

            val SQL_CREATE_ROSTER_TABLE =
                    """
                        CREATE TABLE ${StatsContract.Roster.TABLE_NAME} (
                            ${StatsContract.Roster.COLUMN_NAME_TEAM_ID} INTEGER NOT NULL REFERENCES ${StatsContract.TeamInfo.TABLE_NAME},
                            ${StatsContract.Roster.COLUMN_NAME_SEASON_ID} INTEGER NOT NULL REFERENCES ${StatsContract.SeasonList.TABLE_NAME},
                            ${StatsContract.Roster.COLUMN_NAME_PLAYER_ID} INTEGER NOT NULL REFERENCES ${StatsContract.PlayerInfo.TABLE_NAME},
                            ${StatsContract.Roster.COLUMN_NAME_CAPTAIN} BOOLEAN,
                            PRIMARY KEY (${StatsContract.Roster.COLUMN_NAME_TEAM_ID},
                                         ${StatsContract.Roster.COLUMN_NAME_SEASON_ID},
                                         ${StatsContract.Roster.COLUMN_NAME_PLAYER_ID})
                        ) WITHOUT ROWID;
                    """
        }
    }

    companion object {

        private val AUTHORITY = StatsContract.CONTENT_AUTHORITY

        // Uri IDs
        val ROUTE_TEAM_INFO = 1
        val ROUTE_TEAM_INFO_ID = 2

        val ROUTE_SEASON_LIST = 3
        val ROUTE_SEASON_LIST_ID = 4

        val ROUTE_GAME_STAT = 5
        val ROUTE_GAME_STAT_ID = 6

        val ROUTE_PLAYER_INFO = 7
        val ROUTE_PLAYER_INFO_ID = 8

        val ROUTE_PLAYER_STAT = 9
        val ROUTE_PLAYER_STAT_ID = 10

        val ROUTE_ROSTER = 11
        val ROUTE_ROSTER_PLAYER_INFO = 12
        val ROUTE_ROSTER_TEAM_INFO = 13


        // Uri Matcher
        private val sUriMatcher = UriMatcher(UriMatcher.NO_MATCH)

        init {

            sUriMatcher.addURI(AUTHORITY, StatsContract.PATH_TEAM_INFO, ROUTE_TEAM_INFO)
            sUriMatcher.addURI(AUTHORITY, StatsContract.PATH_TEAM_INFO.plus("/#"), ROUTE_TEAM_INFO_ID)

            sUriMatcher.addURI(AUTHORITY, StatsContract.PATH_SEASON, ROUTE_SEASON_LIST)
            sUriMatcher.addURI(AUTHORITY, StatsContract.PATH_SEASON.plus("/#"), ROUTE_SEASON_LIST_ID)

            sUriMatcher.addURI(AUTHORITY, StatsContract.PATH_GAME_STAT, ROUTE_GAME_STAT)
            sUriMatcher.addURI(AUTHORITY, StatsContract.PATH_GAME_STAT.plus("/#"), ROUTE_GAME_STAT_ID)

            sUriMatcher.addURI(AUTHORITY, StatsContract.PATH_PLAYER_STAT, ROUTE_PLAYER_STAT)
            sUriMatcher.addURI(AUTHORITY, StatsContract.PATH_PLAYER_STAT.plus("/#/#"), ROUTE_PLAYER_STAT_ID)

            sUriMatcher.addURI(AUTHORITY, StatsContract.PATH_PLAYER_INFO, ROUTE_PLAYER_INFO)
            sUriMatcher.addURI(AUTHORITY, StatsContract.PATH_PLAYER_INFO.plus("/#"), ROUTE_PLAYER_INFO_ID)

            sUriMatcher.addURI(AUTHORITY, StatsContract.PATH_ROSTER, ROUTE_ROSTER)
            sUriMatcher.addURI(AUTHORITY, StatsContract.PATH_ROSTER + "/" +
                                                StatsContract.PATH_ROSTER_PLAYER, ROUTE_ROSTER_PLAYER_INFO)
            sUriMatcher.addURI(AUTHORITY, StatsContract.PATH_ROSTER + "/" +
                                                StatsContract.PATH_ROSTER_TEAM, ROUTE_ROSTER_TEAM_INFO)
        }
    }
}
